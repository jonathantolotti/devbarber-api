<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [
                'create',
                'login',
                'unauthorized'
            ]
        ]);
    }

    /**
     * Realiza a criação de um novo usuário.
     *
     * @param Request $request
     * @return array
     */
    public function create(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (! $validator->fails()) {
            $name = $request->input('name');
            $email = $request->input('email');
            $password = $request->input('password');

            if ((User::where('email', $email)->count()) > 0) {
                return ['error' => 'E-mail já cadastrado, caso tenha esquecido a senha clique em recuperar.'];
            }

            User::create([
                'name' => $name,
                'email' => $email,
                'password' => password_hash($password, PASSWORD_DEFAULT)
            ]);

            $token = Auth()->attempt([
                'email' => $email,
                'password' => $password
            ]);

            return [
                'error' => null,
                'avatar' => url('media/avatars/',auth()->user()->avatar),
                'data' => auth()->user(),
                'token' => $token
            ];
        }

        return ['error' => 'Dados inválidos, revise os dados enviados.'];
    }

    /**
     * Realiza login do usuário.
     *
     * @param Request $request
     * @return array
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (! $validator->fails()) {
            $token = auth()->attempt([
                'email' => $request->input('email'),
                'password' => $request->input('password')
            ]);

            if ($token) {
                return [
                    'error' => null,
                    'avatar' => url('media/avatars/',auth()->user()->avatar),
                    'data' => auth()->user(),
                    'token' => $token
                ];
            }
            return ['error' => 'E-mail e/ou senha inválidos.'];
        }

        return ['error' => 'Dados inválidos, revise as informações enviadas.'];
    }

    /**
     * Realiza logout do usuário.
     * @return array
     */
    public function logout()
    {
        auth()->logout();
        return ['error' => null];
    }

    /**
     * Atualiza o token JWT do usuário.
     *
     * @return array
     */
    public function refresh()
    {
        return [
            'error' => null,
            'avatar' => url('media/avatars/',auth()->user()->avatar),
            'data' => auth()->user(),
            'token' => auth()->refresh()
        ];
    }

    /**
     * Retorna erro de usuário não autenticado.
     *
     * @return JsonResponse
     */
    public function unauthorized()
    {
        return response()->json(['error' => 'Não autorizado'], 401);
    }
}
