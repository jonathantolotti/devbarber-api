<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BarberController;
use App\Http\Controllers\UserController;

Route::get('/401', [AuthController::class, 'unauthorized'])->name('login');

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('user', [AuthController::class, 'create']);
});

Route::prefix('user')->group(function () {
    Route::get('/', [UserController::class, 'read']);
    Route::put('/', [UserController::class, 'update']);
    Route::get('favorites', [UserController::class, 'getFavorites']);
    Route::post('favorite', [UserController::class, 'createFavorite']);
    Route::get('appointments', [UserController::class, 'getAppointments']);
});

Route::prefix('barber')->group(function () {
    Route::get('/', [BarberController::class, 'list']);
    Route::get('/{id}', [BarberController::class, 'one']);
    Route::get('/{id}/appointment', [BarberController::class, 'appointment']);
});

Route::get('/search', [BarberController::class, 'search']);





